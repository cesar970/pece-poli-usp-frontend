<div class="remodal remodal-single p-4" data-remodal-options="hashTracking: false" data-remodal-id="modal"><div class="col-md-11 d-flex flex-wrap align-items-center h-100 w-100 m-auto"><button data-remodal-action="close" class="remodal-close"></button><div id="first-c"><h3 class="text-center m-auto pb-5 pt-5 pt-md-2"><div class="color-greyd pb-3 pb-md-0">PRÉ INSCRIÇÃO - <?php
                    $post_type = get_post_type();

                    switch ($post_type) {
                        case "mba":
                            $post_title = "MBA";
                            break;
                        case "especializacao":
                            $post_title = "Especialização";
                            break;
                        case "ead":
                            $post_title = get_field("tipo");
                            break;
                        case "outroscursos":
                            $post_title = get_field("tipo");
                            break;
                    }

                    echo $post_title;

                    ?></div><span class="color-blued font-weight-bold"> <?= get_the_title() ?> </span></h3><form id="first-form" class="toggle-disabled"><input type="hidden" name="codigocurso" value="<?= the_field('codigo'); ?>"> <input type="hidden" name="infocurso" value="<?php $post_type = get_post_type(); switch ($post_type) { case "mba": $post_title = "MBA"; break; case "especializacao": $post_title = "Especialização"; break; case "ead": $post_title = get_field("tipo"); break; case "outroscursos": $post_title = get_field("tipo"); break; } echo $post_title; ?> - <?= get_the_title() ?> "><div class="row"><div class="col-md-6"><div class="form-group"><input type="text" class="form-control" data-validation="cpf" name="cpffirstform" required data-mask="000.000.000-00" name="text" placeholder="CPF"></div></div><div class="col-md-6"><div class="form-group"><input type="text" data-validation="custom" name="nomefirstform" maxlength="10" data-validation-error-msg="Digite apenas letras" data-validation-error-msg-container="#nome-error-dialog" data-validation-regexp="^([a-zA-Z ]+)$" class="form-control" required placeholder="Nome completo"> <span id="nome-error-dialog"></span></div></div><div class="col-md-7"><div class="form-group"><input type="email" class="form-control" name="emailfirstform" data-validation="email" required name="email" placeholder="E-mail"></div></div><div class="col-md-5"><div class="form-group"><input type="text" class="form-control" data-validation="url" maxlength="200" id="linkedinfirstform" name="linkedinfirstform" placeholder="Linkedin"></div></div><div class="col-12"><input type="submit" id="submit-form" class="bg-yellow color-blued border-0 send-single" value="ENVIAR"></div><div class="col-12 d-nonee" id="loading-icon"><img width="68px" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/load-pacman.svg" alt="Pacman"></div></div></form></div><div id="callback-form" class="m-auto d-nonee"><div class="mb-5"></div><div class="mb-4"></div><div class="detail-yellow bg-grey m-auto"></div><p class="color-greyd mt-4 aguarde"></p></div><hr class="w-100"><p class="text-left"><span class="color-blued font-weight-bold">Dúvidas?</span><br>Vamos adorar conversar com você!<br>Envie um email para <b><a href="mailto:atendimento@pecepoli.com.br">atendimento@pecepoli.com.br</a></b> ou ligue para <b><a href="tel:+5501129980000">(11) 2998-0000</a></b></p></div></div>