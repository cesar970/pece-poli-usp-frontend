<!DOCTYPE html><html lang="pt_BR"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title> <?php

        if (is_home()):

            echo 'Pece Poli USP';

        elseif (is_category()):

            single_cat_title();

        elseif (is_single()):

            single_post_title();

        elseif (is_page()):

            single_post_title();

        else:

            wp_title('', true);

        endif;

        ?> </title><meta name="robots" content="index, follow"><style type="text/css"> <?php

        $url = get_bloginfo('template_directory') . '/dist/css/style.css';

        $ch = curl_init();

        curl_setopt ($ch, CURLOPT_URL, $url);

        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);

        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

        $contents = curl_exec($ch);

        if(curl_errno($ch)):

            echo curl_error($ch);

            $contents = '';

        else:

            curl_close($ch);

        endif;

        if(!is_string($contents) || !strlen($contents)):



            $contents = '';

    endif;



    $contents = str_replace('../img/', get_bloginfo('template_directory') . '/dist/img/', $contents);



    $contents = str_replace('../fonts/', get_bloginfo('template_directory') . '/dist/fonts/', $contents);



    echo $contents;

    ?> </style> <?php wp_head();

    function logoChange()
    {

        if (is_front_page() || is_single() || is_page('videos')) {
            return get_stylesheet_directory_uri() . '/dist/img/logo.png';
        } else {
            return get_stylesheet_directory_uri() . '/dist/img/logo-blue.png';
        }
    }

    ?> </head><body data-url="<?php if(is_single()){ $urlcode = 'single'; echo $urlcode;} else{$urlcode = get_the_permalink(); echo $urlcode; }?>"><header id="menu-pece" class="pt-3"><div class="container"><div class="row align-items-center pb-3"><div class="col-5 col-md-4 d-md-flex align-items-center"><div class="col-md-2"><div id="toggle"><span></span></div></div><div class="col-md-6 d-none d-lg-flex social-header"><a href="https://www.facebook.com/pecepoli/" target="_blank"><div class="icon-bg hover-yellow"><i class="fab fa-facebook-f"></i></div></a><a href="https://twitter.com/PECEPoli" target="_blank"><div class="icon-bg hover-yellow"><i class="fab fa-twitter"></i></div></a><a href="https://www.youtube.com/channel/UCrlvDFZTOBQqD8wyplr-hLg/feed" target="_blank"><div class="icon-bg hover-yellow"><i class="fab fa-youtube"></i></div></a><a href="https://www.linkedin.com/company/pecepoli" target="_blank"><div class="icon-bg hover-yellow"><i class="fab fa-linkedin-in"></i></div></a></div></div><div class="logo col-7 col-md-4 pr-0 text-center"><a href="<?php echo get_home_url(); ?>"><img class="pr-3" src="<?php echo logoChange(); ?> " alt="Logo PECE - Escola Politécnica da USP"></a></div><div class="col-md-4 d-none d-md-block"><a href="<?php echo home_url(); ?>/area-do-aluno/" class="hover-blue float-right bg-brown font-weight-bold p-3 pl-4 pr-4 text-white">ÁREA DO ALUNO <i class="ml-2 fas fa-arrow-right"></i></a></div></div><div class="d-none d-md-block mt-4"><div class="row justify-content-center sub-menu align-items-center" id="sub-menu"><a href="<?php echo home_url(); ?>/institucional"><div>QUEM SOMOS</div></a><a data-remodal-target="modal-mba" href="#" class="dropdown-toggle">MBA </a><a data-remodal-target="modal-especializacao" href="#"><div class="dropdown-toggle">ESPECIALIZAÇÃO</div></a><a href="<?php echo home_url(); ?>/ead/">EAD </a><a data-remodal-target="modal-outros" href="#"><div class="dropdown-toggle">OUTROS CURSOS</div></a></div></div></div><div id="menu"><ul><li><a href="<?php echo home_url(); ?>/area-do-aluno/" class="font-weight-bold firts-list d-md-none"><span>Área do aluno</span></a></li><li><a href="<?php echo home_url(); ?>/"><span>Home</span></a></li><li><a href="<?php echo home_url(); ?>/institucional"><span>Quem somos</span></a></li><li class="d-md-none"><a data-remodal-target="modal-mba" href="" class="dropdown-toggle"><span>MBA</span></a></li><li class="d-md-none"><a data-remodal-target="modal-especializacao" href="#" class="dropdown-toggle"><span>Especialização</span></a></li><li class="d-md-none"><a href="<?php echo home_url(); ?>/ead/"><span>EAD</span></a></li><li class="d-md-none"><a data-remodal-target="modal-outros" href="#" class="dropdown-toggle"><span>Outros Cursos</span></a></li><li><a href="<?php echo home_url(); ?>/blog/"><span>Blog</span></a></li><li><a href="<?php echo home_url(); ?>/eventos-e-palestras"><span>Eventos & Palestras</span></a></li><li><a href="<?php echo home_url(); ?>/monografias"><span>Monografias</span></a></li><li><a href="<?php echo home_url(); ?>/videos/" title="Vídeos"><span>Vídeos</span></a></li><li><a href="<?php echo home_url(); ?>/faq"><span>Faq</span></a></li><li><a href="<?php echo home_url(); ?>/contato/"><span>Contato</span></a></li><li class="mt-5 mb-5 d-none d-md-block"><a href="https://sistemapece.pecepoli.com.br/LoginProf.aspx" title="Portal do Prodessor"><span class="border color-brown p-3 pl-5 pr-5 font-weight-bold">PORTAL DO PROFESSOR</span></a></li><li><div class="container text-md-left"><hr class="mt-4 mb-4 d-md-none"><div class="d-flex align-items-center"><i class="fas fa-phone color-brown mr-1"></i> <a href="tel:+551129980000" class="color-greyd">(11) 2998-0000</a></div><div class="d-flex align-items-center"><i class="far fa-envelope color-brown mr-1"></i> <a href="mailto:tendimento@pecepoli.com.br" class="text-lowercase">atendimento@pecepoli.com.br</a></div></div></li></ul></div><div class="remodal" data-remodal-options="hashTracking: false" data-remodal-id="modal-mba"><button data-remodal-action="close" class="remodal-close"></button><div class="content-curso"> <?php

            $args = array(
                'post_type' => array('mba'),
                'orderby' => 'title',
                'order' => 'ASC',
                'nopaging' => true

            );

            $produtos_widget = new WP_Query($args);

            if ($produtos_widget->have_posts()) {

                while ($produtos_widget->have_posts()) {

                    $produtos_widget->the_post();

                    ?> <div class="link"><a href="<?= get_permalink() ?>" title="<?= the_title() ?>"><?= the_title() ?></a></div> <?php
                }

                wp_reset_postdata();
            }
            ?> </div></div><div class="remodal" data-remodal-options="hashTracking: false" data-remodal-id="modal-outros"><button data-remodal-action="close" class="remodal-close"></button><div class="content-curso"> <?php

            $args = array(
                'post_type' => array('outroscursos'),
                'orderby' => 'title',
                'order' => 'ASC',
                'nopaging' => true

            );

            $produtos_widget = new WP_Query($args);

            if ($produtos_widget->have_posts()) {

                while ($produtos_widget->have_posts()) {

                    $produtos_widget->the_post();

                    ?> <div class="link"><a href="<?= get_permalink() ?>" title="<?= the_title() ?>"><?= the_title() ?></a></div> <?php
                }

                wp_reset_postdata();
            }
            ?> </div></div><div class="remodal" data-remodal-options="hashTracking: false" data-remodal-id="modal-especializacao"><button data-remodal-action="close" class="remodal-close"></button><div class="content-curso"> <?php

            $args = array(
                'post_type' => array('especializacao'),
                'orderby' => 'title',
                'order' => 'ASC',
                'nopaging' => true

            );

            $produtos_widget = new WP_Query($args);

            if ($produtos_widget->have_posts()) {

                while ($produtos_widget->have_posts()) {

                    $produtos_widget->the_post();

                    ?> <div class="link"><a href="<?= get_permalink() ?>" title="<?= the_title() ?>"><?= the_title() ?></a></div> <?php
                }

                wp_reset_postdata();
            }
            ?> </div></div></header></body></html>