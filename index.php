<?php get_header() ?> <section><div class="slide-home d-flex text-center align-items-center justify-content-between container-fluid"><div class="container text-md-left mt-md-5"><div class="col-12"><div class="row align-items-center mt-md-5 pt-md-5 desktop-course"><div class="col-lg-4 col-md-12 mt-md-5"><h1>Invista em sua transformação profissional</h1><span class="detail"></span><p>As principais escolhas para<br>uma carreira de sucesso</p></div><div class="col-lg-8 col-md-12 d-none d-md-block"><div class="owl-default owl-carousel owl-theme position-relative"> <?php

                                function OtherCoursesStyleFix($tipo){

                                    switch ($tipo) {
                                        case "MBA":
                                            $post_class = "mba";
                                            break;
                                        case "Especialização":
                                            $post_class = "especializacao";
                                            break;
                                        case "Difusão":
                                            $post_class = "difusao";
                                            break;
                                        case "Atualização":
                                            $post_class = "atualizacao";
                                            break;
                                    }

                                    return $post_class;

                                }

                                $args = array(
                                    'post_type' => array('mba', 'especializacao', 'ead', 'outroscursos'),
                                    'orderby' => 'rand',
                                    'posts_per_page' => '12',

                                );

                                $produtos_widget = new WP_Query($args);

                                if ($produtos_widget->have_posts()) {

                                    $graduacao_index = 0;

                                    while ($produtos_widget->have_posts()) {

                                        $produtos_widget->the_post();

                                        $graduacao_index++;

                                        if($graduacao_index == 1){

                                            echo '<div class="item d-flex flex-wrap text-white">';

                                        }

                                        $post_type = get_post_type();

                                        switch ($post_type) {
                                            case "mba":
                                                $post_class = "mba";
                                                $post_title = "MBA";
                                                break;
                                            case "especializacao":
                                                $post_class = "especializacao";
                                                $post_title = "Especialização";
                                                break;
                                            case "ead":
                                                $post_class = OtherCoursesStyleFix(get_field("tipo"));
                                                $post_title = get_field("tipo");
                                                break;
                                            case "outroscursos":
                                                $post_class = OtherCoursesStyleFix(get_field("tipo"));
                                                $post_title = get_field("tipo");
                                                break;
                                        }

                                        ?> <a class="box-item col-md-4 text-white" href="<?= get_permalink() ?>"><div class="info"><span class="dot-<?=$post_class;?>">⚈</span> <?=$post_title;?></div><div class="info-title"><h2><?php the_title(); ?></h2></div></a> <?php
                                        if($graduacao_index == 6){

                                            $graduacao_index = 0;

                                            echo '</div>';

                                        }
                                    }

                                    wp_reset_postdata();
                                }
                                ?> </div></div></div></div></div></div></section><section><div class="collapse-mobile d-md-none"><!--                <div class="card-header" id="headingOne">--><!----><!--                    <div class="container-fluid pt-2 pb-2">--><!----><!--                        <div class="d-flex flex-wrap align-items-center" data-toggle="collapse"--><!--                             data-target="#collapseone"--><!--                             aria-expanded="true"--><!--                             aria-controls="collapseone">--><!----><!----><!--                            <div class="col-10 p-0">--><!--                                <small>Especialização</small>--><!----><!--                                <br>--><!----><!--                                <b>Engenharia de--><!--                                    Segurança do Trabalho--><!--                                </b>--><!--                            </div>--><!----><!--                            <div class="col-2 p-0">--><!----><!--                                <a href="" class="text-white icon-link"><i class="fas fa-arrow-right"></i></a>--><!----><!--                            </div>--><!----><!--                        </div>--><!----><!--                    </div>--><!----><!--                    <div id="collapseone" class="collapse" aria-labelledby="headingOne"--><!--                         data-parent="#accordionExample">--><!----><!--                        <div class="card-body container">--><!----><!--                            <div class="d-flex">--><!----><!--                                <div class="col-9">--><!----><!--                                    Abordagem multidisciplinar, o aluno irá compreender a gênese das Tecnologias da--><!--                                    Informação.--><!----><!--                                </div>--><!----><!--                                <div class="col-3 mt-2">--><!----><!----><!--                                </div>--><!----><!--                            </div>--><!----><!--                        </div>--><!----><!--                    </div>--><!----><!--                </div>--> <?php

                function OtherFix($tipo){

                    switch ($tipo) {
                        case "MBA":
                            $post_class = "mba";
                            break;
                        case "Especialização":
                            $post_class = "especializacao";
                            break;
                        case "Difusão":
                            $post_class = "difusao";
                            break;
                        case "Atualização":
                            $post_class = "atualizacao";
                            break;
                    }

                    return $post_class;

                }

                $args = array(
                    'post_type' => array('mba', 'especializacao', 'ead', 'outroscursos'),
                    'orderby' => 'rand',
                    'posts_per_page' => '5',

                );

                $produtos_widget = new WP_Query($args);

                if ($produtos_widget->have_posts()) {

                    $graduacao_index = 0;

                    while ($produtos_widget->have_posts()) {

                        $produtos_widget->the_post();

                        $graduacao_index++;

                        if($graduacao_index == 1){

                            echo '  <div class="accordion text-white" id="accordionExample">';

                        }

                        $post_type = get_post_type();

                        switch ($post_type) {
                            case "mba":
                                $post_class = "mba";
                                $post_title = "MBA";
                                break;
                            case "especializacao":
                                $post_class = "especializacao";
                                $post_title = "Especialização";
                                break;
                            case "ead":
                                $post_class = OtherFix(get_field("tipo"));
                                $post_title = get_field("tipo");
                                break;
                            case "outroscursos":
                                $post_class = OtherFix(get_field("tipo"));
                                $post_title = get_field("tipo");
                                break;
                        }

                        ?> <div class="card-header" id="headingOne" onclick="location.href='<?= get_the_permalink(); ?>';"><div class="container-fluid pt-2 pb-2"><div class="d-flex flex-wrap align-items-center" data-toggle="collapse" data-target="#collapseone" aria-expanded="true" aria-controls="collapseone"><div class="col-10"><small><?=$post_title;?></small><br><b><?php the_title(); ?> </b></div><div class="col-2 p-0"><div class="text-white icon-link"><i class="fas fa-arrow-right"></i></div></div></div></div></div><!--                        <a class="box-item col-md-4 text-white" href="--><?//= get_permalink() ?><!--">--><!----><!----><!----><!--                            <div class="info"><span class="dot---> <?//=$post_class;?><!--">⚈</span> </div>--><!----><!--                            <div class="info-title">--><!----><!--                                <h2></h2>--><!----><!--                            </div>--><!----><!----><!----><!--                        </a>--> <?php
                        if($graduacao_index == 6){

                            $graduacao_index = 0;

                            echo '</div>';

                        }
                    }

                    wp_reset_postdata();
                }
                ?> <!--                <div class="card-header" id="headingOne">--><!----><!--                    <div class="container-fluid pt-2 pb-2">--><!----><!--                        <div class="d-flex flex-wrap align-items-center" data-toggle="collapse"--><!--                             data-target="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">--><!----><!--                            <div class="col-11 p-0">--><!--                                <small>Especialização</small>--><!----><!--                                <br>--><!----><!--                                <b>Engenharia de--><!--                                    Segurança do Trabalho--><!--                                </b>--><!--                            </div>--><!----><!--                            <div class="col-1 p-0">--><!----><!--                                <i class="fas fa-plus"></i>--><!----><!--                            </div>--><!----><!--                        </div>--><!----><!--                    </div>--><!----><!--                    <div id="collapsetwo" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true"--><!--                         aria-controls="collapsetwo" class="collapse" aria-labelledby="headingOne"--><!--                         data-parent="#accordionExample">--><!----><!--                        <div class="card-body container">--><!----><!--                            Abordagem multidisciplinar, o aluno irá compreender a gênese das Tecnologias da Informação.--><!----><!--                        </div>--><!----><!--                    </div>--><!----><!--                </div>--></div></section><section><div class="container pt-5 pb-4"><div class="col-12"><div class="row align-items-center justify-content-between"><div class="col-md-4 mb-5 mb-md-0"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/pece-usp.png" alt="Logo pece e usp"></div><div class="col-md-7 border-right"><p>Com o objetivo de difundir o conhecimento da Poli junto ao setor produtivo, atendendo assim aos reclamos da sociedade civil na formação qualificada de seus recursos humanos e a uma das principais atividades fins da Universidade, que é a Extensão.</p></div></div></div></div></section><section><div class="container potencial mt-md-5 mb-md-5 pb-md-5"><div class="col-12"><div class="row justify-content-between"><div class="col-md-7 text-center text-md-left"><div class="spotlight"><h2 class="font-weight-bold color-blued">Explore seu potencial, faça a diferença com a PECE</h2></div><p class="mt-4">Trabalhamos com uma abrangente seleção de programas de educação continuada, que oferece uma gama de oportunidades para o aprendizado qualitativo do aluno. Nossos cursos te levam onde sempre sonhou!</p><div class="row text-left"><div class="item col-10 m-auto col-md-6"><span>+40</span><h2>mil pessoas<br>formadas</h2></div><div class="item col-10 m-auto col-md-6"><span>USP</span><h2>professores<br>capacitados</h2></div><div class="item col-10 m-auto col-md-6"><span>45</span><h2>anos no<br>mercado</h2></div><div class="item col-10 m-auto col-md-6"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/certificado-usp.png" alt="Certificado USP"></span><h2>certificados<br>da USP</h2></div><div class="item col-10 m-auto col-md-6"><span>20</span><h2>cursos de<br>diferentes áreas</h2></div><div class="col-12 text-center mt-5"><a href="" class="btn-y color-blued">SAIBA MAIS</a></div></div></div><div class="col-md-5 d-none d-md-block"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/img-explore-potencial.jpg"></div></div></div></div></section><section><div class="detail-bg"></div><div class="mt-5 pt-5 grow"><div class="container"><div class="d-flex flex-wrap-reverse"><div class="col-md-7 mt-4"><iframe width="100%" src="https://www.youtube.com/embed/jJSBXMVJjww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div><div class="col-md-5 mt-md-5 pt-md-5"><div class="col-md-6 pl-0"><h2 class="color-blued">Cresça em seu setor</h2></div><p class="mb-md-5 pb-md-5">Torne-se um líder produtivo no mercado.</p><a href="" class="color-blued">CONHEÇA A ESCOLA <i class="fas ml-3 fa-arrow-right color-yellow"></i></a><div class="detail-grey d-none d-md-block"></div></div></div></div></div></section><section> <?php include ('news-letter-widget.php')?> </section><section><div class="subscribe bg-blueo pt-5 pb-5 pt-md-0 pb-md-0"><div class="d-flex flex-wrap align-items-center"><div class="owl-default owl-carousel text-white owl-theme position-relative"><div class="item m-auto row align-items-center"><div class="col-md-5 m-auto"><h2 class="font-weight-bold">Inscrições Abertas!<br>Gestão Integrada de Resíduos Sólidos</h2><div class="detail-yellow"></div><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since, is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy.</p><div class="mt-5"><a href="/institucional" class="btn-by text-white">SAIBA MAIS</a></div></div><div class="col-md-5 pr-0 m-r-auto pl-0 d-none d-md-block"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/img-inscricoes.jpg" alt="Mulher sorrindo"></div></div><div class="item m-auto row align-items-center"><div class="col-md-5 m-auto"><h2 class="font-weight-bold">Inscrições Abertas!<br>Gestão Integrada de Resíduos Sólidos</h2><div class="detail-yellow"></div><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since, is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy.</p><div class="mt-5"><a href="" class="btn-by text-white">SAIBA MAIS</a></div></div><div class="col-md-5 pr-0 m-r-auto pl-0 d-none d-md-block"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/img-inscricoes.jpg" alt="Mulher sorrindo"></div></div></div></div></div></section><section><div class="blog-home"><div class="container"><h2 class="text-center mt-3 mb-3">Últimas postagens</h2><div class="owl-posts owl-carousel owl-theme text-white position-relative"> <?php

                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

                    $args = array(

                        'post_type' => 'post',

                        'order' => 'ASC',

                        'posts_per_page' => '6',

                        'paged' => $paged,

                    );

                    $loop = new wp_query($args);

                    while ($loop->have_posts()):$loop->the_post() ?> <a class="box-all rounded item p-5" href="<?php the_permalink(); ?>"><div class="box-image"><div class="img-post-blog" style="background: url('<?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                     echo $url; ?>') no-repeat center center; background-size:cover;"></div></div><div class="box-info"><div class="titulo-blog-post color-greyd"><small class="date"> <?php echo get_the_date('M D, Y'); ?> <br></small><span> <?php the_title(); ?> </span></div></div></a> <?php endwhile; ?> </div></div></div></section> <?php get_footer() ?>