$("#toggle").click(function () {
    $(this).toggleClass("open");
    $("#menu").toggleClass("opened");
});

$('.btn-collapse').click(function () {

    if ($(this).attr('aria-expanded')) {

        var dataTab = $(this).data("tab");

        $("#" + dataTab).addClass("show");

        $(this).attr('aria-expanded', 'false');

    }

    $(".collapse").removeClass("show");

});

$(document).ready(function () {

    $('.owl-videos').owlCarousel({
        loop: false,
        margin: 30,
        nav: true,
        navText: ["<i class='fas fa-arrow-left next text-white'></i>", "<i class='fas text-white fa-arrow-right ml-3'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    $('.owl-default').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left next text-white'></i>", "<i class='fas text-white fa-arrow-right ml-3'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    $('.owl-posts').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left next text-white'></i>", "<i class='fas text-white fa-arrow-right ml-3'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        }
    });

    $('.owl-structure').owlCarousel({
        loop: true,
        margin: 50,
        nav: true,
        navText: ["<i class='fas fa-arrow-left next color-greyd'></i>", "<i class='fas color-greyd fa-arrow-right ml-3'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });
});

//Count characters css
(function ($) {
    function injector(t, splitter, klass, after) {
        var text = t.text()
            , a = text.split(splitter)
            , inject = '';
        if (a.length) {
            $(a).each(function (i, item) {
                inject += '<span class="' + klass + (i + 1) + '" aria-hidden="true">' + item + '</span>' + after;
            });
            t.attr('aria-label', text)
                .empty()
                .append(inject)

        }
    }


    var methods = {
        init: function () {

            return this.each(function () {
                injector($(this), '', 'char', '');
            });

        },

        words: function () {

            return this.each(function () {
                injector($(this), ' ', 'word', ' ');
            });

        },

        lines: function () {

            return this.each(function () {
                var r = "eefec303079ad17405c889e092e105b0";
                // Because it's hard to split a <br/> tag consistently across browsers,
                // (*ahem* IE *ahem*), we replace all <br/> instances with an md5 hash
                // (of the word "split").  If you're trying to use this plugin on that
                // md5 hash string, it will fail because you're being ridiculous.
                injector($(this).children("br").replaceWith(r).end(), r, 'line', '');
            });

        }
    };

    $.fn.lettering = function (method) {
        // Method calling logic
        if (method && methods[method]) {
            return methods[method].apply(this, [].slice.call(arguments, 1));
        } else if (method === 'letters' || !method) {
            return methods.init.apply(this, [].slice.call(arguments, 0)); // always pass an array
        }
        $.error('Method ' + method + ' does not exist on jQuery.lettering');
        return this;
    };

})(jQuery);


//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches


$(".next").click(function () {

    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("form").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style

    current_fs.animate({'display': 'none'}, {

        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({

                'height': 'fit-content'
            });
            next_fs.css({'left': left, 'display': 'block'});
        },
        duration: 0,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("form").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({'display': 'block'}, {

        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'display': 'none !important'});
        },
        duration: 0,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".celphone")
    .mask("(99) 99999-9999")
    .focusout(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 10) {
            element.mask("(99) 99999-99999");
        } else {
            element.mask("(99) 9999-99999");
        }

    });

$(".next, .previous").click(function () {

    $('html, body').scrollTop(500);

    return false;

});


//based on https://dribbble.com/shots/3913847-404-page

var pageX = $(document).width();
var pageY = $(document).height();
var mouseY = 0;
var mouseX = 0;

$(document).mousemove(function (event) {
    //verticalAxis
    mouseY = event.pageY;
    yAxis = (pageY / 2 - mouseY) / pageY * 300;
    //horizontalAxis
    mouseX = event.pageX / -pageX;
    xAxis = -mouseX * 100 - 100;

    $('.box__ghost-eyes').css({'transform': 'translate(' + xAxis + '%,-' + yAxis + '%)'});

    //console.log('X: ' + xAxis);

});

//Add multiples fields

// $(document).ready(function(){
//     var maxField = 10;
//     var addButton = $('.add_button');
//     var wrapper = $('.field_wrapper');
//     var fieldHTML = '<div class="d-flex flex-wrap">\n\n    <div class="col-md-4">\n\n        <div class="form-group">\n\n            <input type="text" name="field_name[]" value=""/>\n\n        </div>\n\n    </div>\n\n    <div class="col-md-4">\n\n        <div class="form-group">\n\n            <input type="text" name="field_name[]" value=""/>\n\n        </div>\n\n    </div>\n\n    <div class="col-md-4">\n\n        <div class="form-group">\n\n            <input type="text" name="field_name[]" value=""/>\n\n        </div>\n\n    </div>\n\n</div>'; //New input field html
//     var x = 1;
//
//
//     $(addButton).click(function(){
//
//         if(x < maxField){
//             x++;
//             $(wrapper).append(fieldHTML);
//         }
//     });
//
//
//     $(wrapper).on('click', '.remove_button', function(e){
//         e.preventDefault();
//         $(this).parent('div').remove();
//         x--;
//     });
// });


//Sender Form full

var firstForm = function () {

    $.validate({
        form: '#first-form',
        modules: 'location, date, security, file, brazil, toggleDisabled',
        disabledFormFilter: 'form.toggle-disabled',
        lang: 'pt',
        showErrorDialogs: true,

        onSuccess: function () {

            var dataForm = $('#first-form').serialize();

            $.ajax({
                type: "POST",
                url: "http://192.168.0.253/pece/api/inscricao/step-one/",
                data: dataForm,
                beforeSend: function () {
                    $("#loading-icon").fadeIn();
                },
                dataType: 'json',
                timeout: 90000
            }).done(function (data) {

                if (data.status === true) {

                    $("#first-c").fadeOut();

                    setTimeout(function () {

                        $("#callback-form").fadeIn();

                        $("#callback-form .mb-5").html("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 130.2 130.2\"> <circle class=\"path circle\" fill=\"none\" stroke=\"#F8D61C\" stroke-width=\"6\" stroke-miterlimit=\"10\" cx=\"65.1\" cy=\"65.1\" r=\"62.1\"/> <polyline class=\"path check\" fill=\"none\" stroke=\"#F8D61C\" stroke-width=\"6\" stroke-linecap=\"round\" stroke-miterlimit=\"10\" points=\"100.2,40.2 51.5,88.8 29.8,67.5 \"/> </svg>");

                        $("#callback-form .mb-4").html("<h3> <span class='color-blued font-weight-bold'> Sua pré-inscrição <br> foi realizada! </span> </h3>");

                        $("#callback-form .aguarde").html("Aguarde nosso contato via e-mail <br> para a próxima etapa.");

                    }, 500);

                } else {

                    $("#first-c").fadeOut();

                    setTimeout(function () {

                        $("#callback-form").fadeIn();

                        $("#callback-form .mb-5").html("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 130.2 130.2\"> <circle class=\"path circle\" fill=\"none\" stroke=\"#D06079\" stroke-width=\"6\" stroke-miterlimit=\"10\" cx=\"65.1\" cy=\"65.1\" r=\"62.1\"/> <line class=\"path line\" fill=\"none\" stroke=\"#D06079\" stroke-width=\"6\" stroke-linecap=\"round\" stroke-miterlimit=\"10\" x1=\"34.4\" y1=\"37.9\" x2=\"95.8\" y2=\"92.3\"/> <line class=\"path line\" fill=\"none\" stroke=\"#D06079\" stroke-width=\"6\" stroke-linecap=\"round\" stroke-miterlimit=\"10\" x1=\"95.8\" y1=\"38\" x2=\"34.4\" y2=\"92.2\"/> </svg>");

                        $("#callback-form .mb-4").html("<span class='color-blued font-weight-bold'>Ocorreu um erro em sua solicitação <br>Tente novamente em alguns minutos</span>");

                        $("#callback-form .aguarde").html("");

                    }, 500);
                }

            });
            return false;
        }
    });

};

var secondForm = function () {

    $.validate({
        form: '.form-default',
        modules: 'location, date, security, file, brazil, toggleDisabled',
        disabledFormFilter: 'form.toggle-disabled',
        lang: 'pt',
        showErrorDialogs: true,

        onSuccess: function () {

            var dataForm = $('#step1, #step2, #step3, #step4, #step5').serialize();

            $.ajax({
                type: "POST",
                url: "http://192.168.0.253/pece/api/inscricao/step-two/",
                data: dataForm,
                beforeSend: function () {
                    $("#loading-icon").fadeIn();
                },
                dataType: 'json',
                timeout: 90000
            }).done(function (data) {

                if (data.status === true) {

                    $("#all-content").fadeOut();

                    setTimeout(function () {

                        $("#callback-form").fadeIn();

                        $("#callback-form .mb-5").html("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 130.2 130.2\"> <circle class=\"path circle\" fill=\"none\" stroke=\"#F8D61C\" stroke-width=\"6\" stroke-miterlimit=\"10\" cx=\"65.1\" cy=\"65.1\" r=\"62.1\"/> <polyline class=\"path check\" fill=\"none\" stroke=\"#F8D61C\" stroke-width=\"6\" stroke-linecap=\"round\" stroke-miterlimit=\"10\" points=\"100.2,40.2 51.5,88.8 29.8,67.5 \"/> </svg>");

                        $("#callback-form .mb-4").html("<h3> <span class=\"color-blued font-weight-bold\"> Sua inscrição <br> foi realizada! </span> </h3>");

                        $("#callback-form .aguarde").html("Obrigado, aguarde nosso <strong class='color-blueb'> retorno via e-mail </strong> para finalizarmos sua inscrição, desde já agradecemos sua confiança.");

                    }, 500);

                } else {

                    $("#all-content").fadeOut();

                    setTimeout(function () {

                        $("#callback-form").fadeIn();

                        $("#callback-form .mb-5").html("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 130.2 130.2\"> <circle class=\"path circle\" fill=\"none\" stroke=\"#D06079\" stroke-width=\"6\" stroke-miterlimit=\"10\" cx=\"65.1\" cy=\"65.1\" r=\"62.1\"/> <line class=\"path line\" fill=\"none\" stroke=\"#D06079\" stroke-width=\"6\" stroke-linecap=\"round\" stroke-miterlimit=\"10\" x1=\"34.4\" y1=\"37.9\" x2=\"95.8\" y2=\"92.3\"/> <line class=\"path line\" fill=\"none\" stroke=\"#D06079\" stroke-width=\"6\" stroke-linecap=\"round\" stroke-miterlimit=\"10\" x1=\"95.8\" y1=\"38\" x2=\"34.4\" y2=\"92.2\"/> </svg>");

                        $("#callback-form .mb-4").html("<span class=\"color-blued font-weight-bold\">Ocorreu um erro em sua solicitação <br>Tente novamente em alguns minutos</span>");

                        $("#callback-form .aguarde").html("");

                    }, 500);
                }

            });

            return false;
        }
    });

};


var codigocurso = $("#codigo-curso").val();

$.ajax({
    type: "POST",
    url: "http://192.168.0.253/pece/api/inscricao/disciplinas/",
    data: {codigo: codigocurso},
    dataType: "json",
    timeout: 900000,
}).done(function (data) {

    var naoobrigatorias = data.naoobrigatorias;

    var obrigatorias = data.obrigatorias;

    var limit = data.cargasobrando;

    $('body').on('change', 'input.grade-d', function () {

        var inputCustom = $('input.grade-d:checked');

        console.log(limit);

        if (inputCustom.length === limit) {

            var btnform = $("#sendbtnform");

            btnform.removeAttr('disabled');

            btnform.removeClass('disabled');
        }

        if (inputCustom.length > limit) {

            this.checked = false;
        }
    });

    $("#ngrade").html(limit);

    $.each(naoobrigatorias, function (i, item) {

        var nobrigatorias = '<div class="custom-control custom-checkbox">   <input type="checkbox" name="disciplinasn[]" class="custom-control-input grade grade-d" value=" ' + item.codigo + ' - ' + item.nome + '" id="' + item.codigo + '">\n    <label class="custom-control-label" for="' + item.codigo + '"> ' + item.codigo + ' - ' + item.nome + '</label></div>';

        $('#selectd').append(nobrigatorias);

    });

    $.each(obrigatorias, function (i, item) {

        var obrigatorias = '<p>' + item.codigo + ' - ' + item.nome + '</p>';

        $('#obrigatorias').append(obrigatorias);

    });

});

$(".data-row").blur(function () {

    var status;

    if ($(this).val().lenght >= 0) {

        status = 'false';

    } else {

        status = 'true';

    }

    var rowId = $(this).data('row');

    $(".row-" + rowId).attr('data-validation-optional', status);

});


//monografia

var searchMonografia = function () {

    var tableActive = function (sessiondata, tableid) {

        tableid.DataTable({

            data: sessiondata,

            "createdRow": function( row, data, dataIndex) {

                $(row).on('click', function () {

                    window.location.href = "http://192.168.0.253/pece/monografia-download/?content=" + data;

                });

            },

            language: {
                sEmptyTable: "Nenhum registro encontrado",
                sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
                sInfoFiltered: "(Filtrados de _MAX_ registros)",
                sInfoPostFix: "",
                sInfoThousands: ".",
                sLengthMenu: "_MENU_ resultados por página",
                sLoadingRecords: "Carregando...",
                sProcessing: "Processando...",
                sZeroRecords: "Nenhum registro encontrado",
                sSearch: "Pesquisar",
                oPaginate: {
                    sNext: "Próximo",
                    sPrevious: "Anterior",
                    sFirst: "Primeiro",
                    sLast: "Último"
                },

                oAria: {
                    sSortAscending: ": Ordenar colunas de forma ascendente",
                    sSortDescending: ": Ordenar colunas de forma descendente"
                }

            }

        });

    };

    var tableSearch = $("#table-monografia");

    if (sessionStorage.getItem("monografialist") === null) {

        $.ajax({
            type: "POST",
            url: "http://192.168.0.253/pece/api/monografias/monografia-list/",
            dataType: "json",
            timeout: 90000
        }).done(function (data) {

            var monografiaList = data.data;

            sessionStorage.setItem('monografialist', JSON.stringify(monografiaList));

            var sessionMono = JSON.parse(sessionStorage.getItem('monografialist'));

            tableActive(sessionMono, tableSearch);

        });

    } else {

        var sessionMono = JSON.parse(sessionStorage.getItem('monografialist'));

        tableActive(sessionMono, tableSearch);

    }

    // $("#table-monografia tr").on('click', function () {
    //
    //     var sessionMono = JSON.parse(sessionStorage.getItem('monografialist'));
    //
    //     var myTableArray = [];
    //
    //     var arrayOfThisRow = [];
    //
    //     var tableData = sessionMono;
    //
    //     if (tableData.length > 0) {
    //
    //         tableData.each(function() { arrayOfThisRow.push($(this).text()); });
    //
    //         myTableArray.push(arrayOfThisRow);
    //     }
    //
    //     console.log(myTableArray);
    //
    // });


};



