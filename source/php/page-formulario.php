<?php

get_header('formulario');

if (!isset($_GET['token'])) {

    header("Location: " . site_url() . "/formulario-error");

}

$token = $_GET['token'];

$data = json_decode(base64_decode($token));

$timerr = strtotime(date('d/m/Y H:i:s'));

$time = strtotime(date($data->time));

$resultime =  $timerr +  $time;

if ($resultime < 0) {

    header("Location: " . $url . "envio-inscricao-sem-token");

    exit;

}

?>

<div class="formulario-inscricao pb-5 mb-5">

    <div class="container shadow mt-5 py-5">

        <div id="all-content">

            <div class="text-center mb-5">

                <h2 class="color-blued font-weight-bold mb-4">Finalizar inscrição</h2>

                <div class="detail-yellow m-auto"></div>

                <p class="pt-4 mb-0">Siga as etapas para concluir sua inscrição no curso </p>

                <strong class="color-blued"><?= $data->infocurso ?></strong>

            </div>

            <ul id="progressbar" class="d-flex mb-0 justify-content-center">
                <li class="active"></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>

            <div class="form-full col-md-11 m-auto">

                <form id="step1" class="form-default toggle-disabled">

                    <input type="hidden" value="<?= $data->infocurso ?>" name="infocurso">

                    <input type="hidden" id="codigo-curso" value="<?= $data->codigocurso; ?>">

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">DADOS PESSOAIS</p>

                    </div>

                    <div class="form-group">

                        <input type="text" data-validation="custom" data-validation-regexp="^([a-zA-Z ]+)$"
                               class="form-control" name="nome" required
                               placeholder="Nome completo" value="<?= $data->nome ?>">

                    </div>

                    <div class="form-group">

                        <input type="email" class="form-control" data-validation="email" required
                               name="email" placeholder="E-mail" value="<?= $data->email ?>">


                    </div>


                    <div class="form-group">

                        <input type="text" class="form-control" data-validation="cpf" required
                               data-mask="000.000.000-00"
                               name="cpf" placeholder="CPF" value="<?= $data->cpf ?>">

                    </div>

                    <div class="col-12 text-left">

                        <p class="mb-0">Sexo*</p>

                        <div class="custom-control custom-radio custom-control-inline form-group mr-2">

                            <input type="radio" required id="customRadioInline1" value="Masculino" name="sexo"
                                   class="custom-control-input ">
                            <label class="custom-control-label control-label" for="customRadioInline1">Masculino</label>

                        </div>

                        <div class="custom-control custom-radio custom-control-inline form-group">
                            <input type="radio" required id="customRadioInline2" value="Feminino" name="sexo"
                                   class="custom-control-input control-label">
                            <label class="custom-control-label" for="customRadioInline2">Feminino</label>
                        </div>

                    </div>


                    <div class="row align-items-center  mt-md-0">

                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" class="form-control" maxlength="20" required name="ntipodedocumento"
                                       placeholder="RG ou RNE">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" class="form-control" data-validation="birthdate"
                                       data-validation-format="dd/mm/yyyy" required data-mask="00/00/0000"
                                       name="dataexpedicacao" placeholder="Expedição ( / / )">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" maxlength="20" class="form-control" required name="orgaoemissor"
                                       placeholder="Orgão Emissor*">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" class="form-control" data-validation="birthdate"
                                       data-validation-format="dd/mm/yyyy" required data-mask="00/00/0000"
                                       name="datadenascimento"
                                       placeholder="Data Nascimento ( / / )">

                            </div>

                        </div>

                        <div class="col-8 col-md-6">

                            <div class="form-group">

                                <input type="text" maxlength="40" required class="form-control" name="naturaldecidade"
                                       placeholder="Natural de (Cidade)*">

                            </div>

                        </div>

                        <div class="col-4 col-md-2">

                            <select class="custom-select" name="ufdocumento" required>


                                <option selected disabled>UF</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>


                            </select>

                        </div>

                        <div class="col-12 my-4">

                            <small>OBS.: O total de caracteres do endereço + número + complemento não pode ultrapassar
                                38.
                            </small>

                        </div>

                        <div class="col-12 col-md-8">

                            <div class="form-group">

                                <input type="text" class="form-control" maxlength="40" required name="enderecor"
                                       placeholder="Endereço Residencial*">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" maxlength="5" class="form-control" data-validation="number" required
                                       name="numeror"
                                       placeholder="Nº*">

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" maxlength="33" class="form-control" name="complementodados"
                                       placeholder="Complemento">

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" class="form-control" maxlength="30" required name="bairrodados"
                                       placeholder="Bairro*">

                            </div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" class="form-control" name="ceppessoais"
                                       data-validation-error-msg="Digite um número de CEP válido"
                                       data-validation-error-msg-container="#cep-error-dialog" id="ceppessoais"
                                       data-validation-length="min9"
                                       data-validation="length" required data-mask="00000-000"
                                       placeholder="CEP*">

                                <span id="cep-error-dialog"></span>

                            </div>

                        </div>

                        <div class="col-8 col-md-6">

                            <div class="form-group">

                                <input type="text" class="form-control" maxlength="30" required name="cidadedados"
                                       placeholder="Cidade*">

                            </div>

                        </div>

                        <div class="col-md-2 col-4">

                            <select class="custom-select" required name="ufdados">

                                <option selected disabled>UF</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>

                            </select>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="tel" class="form-control"
                                       data-validation-error-msg="Digite um número de telefone válido"
                                       data-validation-error-msg-container="#telefone-error-dialog" required
                                       data-validation="length"
                                       data-validation-length="min13" data-mask="(00)0000-0000"
                                       placeholder="Telefone Residencial*"
                                       name="telefoneresidencial">

                                <span id="telefone-error-dialog"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="tel" class="form-control celphone"
                                       data-validation-error-msg="Digite um número de celular válido"
                                       data-validation-error-msg-container="#celular-error-dialog"
                                       data-validation="length"
                                       data-validation-length="14-16" placeholder="Telefone Celular*"
                                       name="telefonecelular" required>

                                <span id="celular-error-dialog"></span>

                            </div>

                        </div>

                    </div>

                    <input type="submit" class="action-button next" value="AVANÇAR"/>


                </form>

                <form id="step2" class="form-default toggle-disabled">

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">FORMAÇÃO ACADÊMICA</p>

                        <span>
                                                                            Informe o Título da sua Graduação e demais cursos superiores
                                                                            que tenha concluído (Especialização, MBA ou Mestrado)
                                                                        </span>

                    </div>

                    <div class="field_wrapper">

                        <div class="d-flex flex-wrap">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" data-validation="length" data-validation-length="min5" class="row1" name="graduacao[]" required placeholder="Graduação*" value=""/>

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" data-validation="length" data-validation-length="min3"
                                           placeholder="Escola*" class="row1" required name="escola[]" value=""/>

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" data-validation="birthdate" class="row1"
                                           data-validation-format="dd/mm/yyyy" required data-mask="00/00/0000"
                                           placeholder="Data da Colação de Grau"  name="data[]"
                                           value=""/>

                                </div>

                            </div>

                        </div>

                        <div class="d-flex flex-wrap">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" data-row="2" name="graduacao[]" class="data-row row-2" data-validation="length" data-validation-length="min5" placeholder="Formação 2" data-validation-optional="true">

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" name="escola[]" data-row="2" class="data-row row-2"
                                           data-validation="length" data-validation-length="min3" placeholder="Escola"
                                           data-validation-optional="true">

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" name="data[]" data-row="2" class="data-row row-2"
                                           data-mask="00/00/0000"
                                           placeholder="Data Conclusão" data-validation="birthdate"
                                           data-validation-format="dd/mm/yyyy" data-validation-optional="true">

                                </div>

                            </div>

                        </div>

                        <div class="d-flex flex-wrap">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" name="graduacao[]" data-row="3" class="data-row row-3"
                                           data-validation="length" data-validation-length="min5"
                                           placeholder="Formação 3" data-validation-optional="true">

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" name="escola[]" class="data-row row-3" data-row="3"
                                           data-validation="length" data-validation-length="min3" placeholder="Escola"
                                           data-validation-optional="true">

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <input type="text" name="data[]" class="data-row row-3" data-validation="birthdate"
                                           data-validation-format="dd/mm/yyyy" data-row="3" data-mask="00/00/0000"
                                           placeholder="Data Conclusão" data-validation-optional="true">

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">CURSOS COMPLEMENTARES</p>

                        <span>
                                                                     Descreva os cursos feitos nos 2 últimos anos, informando a duração em horas e período de realização (7000 caracteres)
                                                                        </span>

                    </div>


                    <textarea data-validation-error-msg="Descreva os cursos feitos nos 2 últimos anos"
                              data-validation-error-msg-container="#cursocomplementar-error-dialog"
                              data-validation-length="30-7000" data-validation="length"
                              name="cursoscomplementares"
                              required cols="30" rows="5"
                              placeholder="Mensagem*"></textarea>


                    <div class="col-12 text-left mt-4 mb-5">

                        <p>Selecione a seguir seu nível de inglês*</p>

                        <div class="custom-control custom-radio d-md-inline-flex mr-md-3">

                            <input type="radio" id="cursonenhum" value="Nenhum" checked name="nivelingles"
                                   class="custom-control-input">

                            <label class="custom-control-label" for="cursonenhum">Nenhum</label>

                        </div>

                        <div class="custom-control custom-radio d-md-inline-flex mr-md-3">

                            <input type="radio" id="cursobasico" value="Básico" name="nivelingles"
                                   class="custom-control-input">

                            <label class="custom-control-label" for="cursobasico">Básico</label>

                        </div>


                        <div class="custom-control custom-radio d-md-inline-flex mr-md-3">
                            <input type="radio" id="intermediario" value="Intermediário" name="nivelingles"
                                   class="custom-control-input">
                            <label class="custom-control-label" for="intermediario">Intermediário</label>
                        </div>


                        <div class="custom-control custom-radio d-md-inline-flex">

                            <input type="radio" id="avancado" name="nivelingles"
                                   class="custom-control-input" value="Avançado">
                            <label class="custom-control-label" for="avancado">Avançado</label>

                        </div>

                    </div>

                    <input type="submit" name="previous" class="previous action-button" value="VOLTAR"/>

                    <input type="submit" name="next" class="action-button next" value="AVANÇAR"/>


                </form>

                <form id="step3" class="form-default toggle-disabled">

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">HISTÓRICO PROFISSIONAL</p>

                        <span>
                                                                Preencha os campos com os dados do seu empregador atual ou o mais recente
                                                            </span>

                    </div>

                    <div class="row align-items-center">

                        <div class="col-md-12">

                            <div class="form-group">

                                <input type="text" name="instituicaotrabalha" maxlength="60"
                                       placeholder="Instituição/Empresa onde trabalha">

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" name="ramodeatividade" maxlength="40"
                                       placeholder="Ramo de Atividade">

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" name="cargo" placeholder="Cargo" maxlength="40">

                            </div>

                        </div>

                        <div class="col-md-10">

                            <div class="form-group">

                                <input type="text" name="enderecocomercial" maxlength="100"
                                       placeholder="Endereço Comercial">

                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">

                                <input type="text" name="nhistoricoprofissional" data-validation="number" maxlength="6"
                                       placeholder="Nº">

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" name="historicocomplemento" maxlength="40" placeholder="Complemento">

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" name="historicobairro" maxlength="40" placeholder="Bairro">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <input type="text" class="form-control" name="cephistorico"
                                       data-validation-error-msg="Digite um número de CEP válido"
                                       data-validation-error-msg-container="#cephistorico-error-dialog" id="cephistorico"
                                       data-validation-length="min9" data-validation="length" required
                                       data-mask="00000-000" placeholder="CEP*">

                                <span id="cephistorico-error-dialog"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <input type="text" name="historicocidade" maxlength="40" class="form-control"

                                       placeholder="Cidade*">

                            </div>

                        </div>

                        <div class="col-md-2">

                            <select class="custom-select" name="ufhistorico">

                                <option selected disabled>UF</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select>

                        </div>

                        <div class="col-md-7">

                            <div class="form-group">

                                <input type="text" name="historicotelefonecomecial" class="form-control celphone"

                                       placeholder="Telefone Comercial">


                            </div>

                        </div>

                        <div class="col-md-5">

                            <div class="form-group">

                                <input type="text" name="historicoemailempresa" data-validation="email" required
                                       class="form-control"
                                       placeholder="E-mail da Empresa">

                            </div>

                        </div>

                    </div>

                    <div class="form-group">


                                                            <textarea name="atividadesprofissionais" cols="30"
                                                                      rows="5"
                                                                      placeholder="Atividades Profissionais Anteriores"></textarea>

                    </div>

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">QUALIFICAÇÕES PROFISSIONAIS</p>

                        <span>
                                                                Descreva sucintamente suas qualificações profissionais (7000 caracteres)
                                                            </span>

                    </div>


                    <div class="form-group">

                                                            <textarea name="historicodescrevaqualificacoes"

                                                                      data-validation-error-msg-container="#qualificacoes-error-dialog"
                                                                      data-validation-length="30-7000" required
                                                                      data-validation="length"
                                                                      cols="30"
                                                                      rows="5"
                                                                      placeholder="Descreva"></textarea>

                        <span id="qualificacoes-error-dialog"></span>

                    </div>

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">MOTIVO PARA REALIZAR O CURSO*</p>

                        <span>
                                                                Descreva o motivo para realizar o curso (7000 caracteres)
                                                            </span>

                    </div>


                    <div class="form-group">

                                                            <textarea name="historicodescrevarealizar"

                                                                      data-validation-error-msg-container="#motivos-error-dialog"
                                                                      data-validation-length="30-7000"
                                                                      data-validation="length" required
                                                                      cols="30" rows="5"
                                                                      placeholder="Descreva"></textarea>

                        <span id="motivos-error-dialog"></span>

                    </div>

                    <input type="submit" name="previous" class="previous action-button" value="VOLTAR"/>

                    <input type="submit" name="next" class="next action-button" value="AVANÇAR"/>

                </form>

                <form id="step4" class="form-default">

                    <div class="mt-5 mb-4">

                        <p class="fs-title font-weight-bold ">PESQUISA PECE*</p>

                        <span>
                                                                Como você tomou conhecimento dos nossos cursos?
                                                            </span>

                    </div>

                    <div class="row align-items-center text-left">

                        <div class="col-md-4">

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio1" name="indicacao" value="Indicação - EMPRESA"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio1">Indicação - EMPRESA</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio2" name="indicacao"
                                       value="Indicação - PROFESSOR PECE"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio2">Indicação - PROFESSOR PECE
                                </label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio3" value="Indicação - ALUNO PECE"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio3">Indicação - ALUNO PECE </label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio4" name="indicacao" value="Indicação - EX-ALUNO PECE"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio4">Indicação - EX-ALUNO PECE</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio5" value="Outros"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio5">Outros</label>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio6" name="indicacao" value="Anuncio - Jornal"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio6">Anuncio - Jornal</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio7" name="indicacao" value="Anúncio - Revista"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio7">Anúncio - Revista</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio8" name="indicacao" value="Anúncio - Rádio"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio8">Anúncio - Rádio</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio9" name="indicacao" value="Anúncio - Metrô"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio9">Anúncio - Metrô</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio10" name="indicacao" value="LinkedIn"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio10">LinkedIn</label>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio11" name="indicacao" value="Mala direta - FOLHETO"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio11">Mala direta - FOLHETO</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio12" name="indicacao" value="Mala direta - E-MAIL"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio12">Mala direta - E-MAIL</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio13" name="indicacao"
                                       value="Internet - SITE DE PESQUISA"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio13">Internet - SITE DE
                                    PESQUISA</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio14" name="indicacao" value="Facebook"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio14">Facebook</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio15" name="indicacao" value="Twitter"

                                       class="custom-control-input">
                                <label class="custom-control-label" for="customRadio15">Twitter</label>
                            </div>

                        </div>


                    </div>

                    <div class="col-12">

                        <div class="form-group">

                            <input type="text" name="pesquisaoutro" placeholder="Qual(is) outro(s)?">

                        </div>

                    </div>

                    <input type="submit" name="previous" class="previous action-button" value="VOLTAR"/>

                    <input type="submit" name="next"  class="next action-button" value="AVANÇAR"/>


                </form>

                <form id="step5" class="form-default toggle-disabled">

                    <div class="col-md-12 text-left d-flex flex-wrap justify-content-center">

                        <div class="col-md-8 m-auto">

                            <div class="mt-5 mb-4">

                                <p class="fs-title font-weight-bold text-center">DISCIPLINAS OBRIGATÓRIAS</p>

                                <div id="obrigatorias" class="text-left mb-5"></div>

                                <p class="fs-title font-weight-bold text-center">SELEÇÃO DE GRADE DE DISCIPLINAS</p>

                                <span>
                                            Selecione a seguir a grade com as <span id="ngrade"></span> disciplinas quer você deseja cursar

                                        </span>

                            </div>


                            <div id="selectd">


                            </div>

                        </div>

                    </div>

                    <input type="submit" name="previous" class="previous action-button" value="VOLTAR"/>

                    <input type="submit" name="next" class="action-button disabled" id="sendbtnform"
                           disabled="disabled"
                           value="ENVIAR"/>

                    <div class="col-12 d-nonee" id="loading-icon">

                        <img width="68px" class="m-auto"
                             src="
                    <?php echo get_stylesheet_directory_uri(); ?>/dist/img/load-pacman.svg" alt="Pacman">

                    </div>

                </form>

            </div>

        </div>

        <div id="callback-form" class="m-auto d-nonee">

            <div class="mb-5"></div>

            <div class="mb-4 text-center"></div>

            <div class="detail-yellow bg-grey m-auto"></div>

            <p class="color-greyd mt-4 aguarde col-md-6 text-center m-auto pt-3"></p>

            <hr class="w-100 mt-5">

            <p class="text-left pl-md-4"><span class="color-blued font-weight-bold">Dúvidas?</span> <br>
                Vamos adorar conversar com você! <br>
                Envie um email para <b><a href="mailto:atendimento@pecepoli.com.br" class="color-blueb">atendimento@pecepoli.com.br</a></b>
                ou ligue para <b>
                    <a href="tel:+5501129980000" class="color-blueb">(11) 2998-0000</a> </b></p>

        </div>

    </div>

</div>

<?php get_footer('formulario') ?>

