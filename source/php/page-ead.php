<?php get_header(); ?>

<div class="ead pb-5">

    <div class="container">

        <div class="side-title mt-5 text-center">

            <h2 class="color-blackd font-weight-bold">

                EAD

            </h2>

            <h3 class="mb-4">Cursos a Distância</h3>

            <div class="detail-yellow m-auto"></div>

        </div>

        <div class="pt-4 text-center">

            <p>Confira abaixo a relação dos nossos cursos a distância:</p>

        </div>

        <?php

        $wp_query = new WP_Query();

        query_posts(array('post_type' => 'ead', 'showposts' => -1, 'paged' => $paged));

        if (have_posts()):

            $tipoEad = get_field_object('tipo', 265)['choices'];

            $eadCursos = array();

            while ($wp_query->have_posts()) : $wp_query->the_post();

                foreach ($tipoEad as $listEad) {

                    if (!isset($eadCursos[$listEad])) {
                        $eadCursos[$listEad] = array();
                    }

                    if (get_field('tipo') == $listEad) {

                        $curso = array(
                            "titulo" => get_the_title(),
                            "imagem" => get_field('imagem'),
                            "link" => get_permalink()
                        );

                        array_push($eadCursos[$listEad], $curso);

                    }

                }

            endwhile;

        else:

        endif; ?>

        <?php

        foreach ($eadCursos as $key => $value) { ?>

            <div class="row">

                <div class="cat mt-5 mb-3 col-12">

                    <h3><?= $key ?></h3>

                </div>

                <?php

                foreach ($value as $values) { ?>

                    <div class="col-md-4 text-white">

                        <div class="img-post"
                             style="background: url('<?= $values['imagem'] ?>') center center no-repeat;">

                        </div>

                        <div class="box-item">

                            <div class="titulo font-weight-bold">

                                <?= $values['titulo'] ?>

                            </div>

                            <a href="<?= $values['link'] ?>" class="text-white font-weight-bold">CONHEÇA A ESCOLA

                                <i class="fas ml-3 fa-arrow-right color-yellow"></i>

                            </a>

                        </div>

                    </div>

                <?php } ?>

            </div>

        <?php } ?>

    </div>

</div>

<?php get_footer(); ?>
