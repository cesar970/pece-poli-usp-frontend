<footer>

    <div class="bg-black pt-3 pb-5 text-center text-md-left">

        <div class="container">

            <div class="col-12">

                <hr class="bg-light mb-4 mt-5">

            </div>

            <div class="d-md-flex flex-wrap">

                <div class="col-md-4 d-flex flex-wrap align-items-center">

                    <div class="col-12 mt-3 mb-4 mb-md-3">

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-pece-white.png" alt="">

                    </div>

                    <div class="col-12">

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-escola-politecnica.png"
                             alt="">

                    </div>

                    <div class="col-12 mt-4">

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-usp.png" alt="">

                    </div>

                </div>

                <div class="col-md-8 d-flex flex-wrap text-left mt-4">

                    <div class="col-7 col-md-4 pr-0">

                        <ul>

                            <li class="color-brown mb-1">MAPA DO SITE</li>

                            <a href="">
                                <li>Institucional</li>
                            </a>

                            <a href="#" data-remodal-target="modal-mba">
                                <li>MBA</li>
                            </a>

                            <a href="#" data-remodal-target="modal-especializacao">
                                <li>Especializações</li>
                            </a>

                            <a href="<?= get_home_url(); ?>/ead">
                                <li>EAD</li>
                            </a>

                            <a href="<?= get_home_url(); ?>/eventos-e-palestras">
                                <li>Eventos & Palestras</li>
                            </a>

                            <a href="<?= get_home_url(); ?>/blog">
                                <li>Postagens</li>
                            </a>

                            <a href="<?= get_home_url(); ?>/area-do-aluno">
                                <li>Área do aluno</li>
                            </a>

                            <a href="<?= get_home_url(); ?>/videos">
                                <li>Vídeos</li>
                            </a>

                            <a href="<?= get_home_url(); ?>/faq">
                                <li>FAQ</li>
                            </a>

                            <!--                            <a href="">-->
                            <!--                                <li>Parceiros</li>-->
                            <!--                            </a>-->

                        </ul>

                    </div>

                    <div class="col-5 col-md-3 pl-0">

                        <span class="color-brown">SOCIAL</span> <br>

                        <div class="d-flex mt-2">

                            <a href="https://www.facebook.com/pecepoli/" class="icon-footer mr-1 hover-yellow" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>

                            <a href="https://www.linkedin.com/company/pecepoli" class="icon-footer mr-1 hover-yellow" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>

                            <a href="https://twitter.com/PECEPoli" class="icon-footer hover-yellow" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>

                        </div>

                    </div>

                    <div class="col-12 col-md-5">

                        <span class="color-brown mb-1">CONTATO</span>

                        <p class="text-white">Av. Prof. Mello Moraes, nº 2373, 1o. andar Cidade Universitária  Campus
                            Armando de Salles
                            Oliveira
                        </p>

                        <p class="text-white">
                            (11) 2998-0000 <br>
                            atendimento@pecepoli.com.br</p>

                    </div>

                    <div class="col-12 text-center text-md-right mt-4">

                        <small class="text-white">©Copyright <?php echo date('Y'); ?> PECE - Todos direitos reservados.
                        </small>

                    </div>

                </div>

            </div>

        </div>

    </div>

</footer>

<!-- END FOOTER -->

<?php wp_footer(); ?>


<?php

if (is_singular('post')) {

    echo '   <script type="text/javascript">     
     $("#toggle").addClass("toggle-black"); 
     
     $("#sub-menu").addClass("black-menu");
     </script>';
}

if (is_front_page() || is_single() || is_page('videos')) {

    echo '
    <script type="text/javascript">
    $("#menu-pece").addClass("position-md-absolute");
    </script>';

} else {
    echo '    <script type="text/javascript">     
     $("#toggle").addClass("toggle-black"); 
     
     $("#sub-menu").addClass("black-menu");
     </script>';
}

if (is_page('formulario-completo')) {

    echo '    <script type="text/javascript">     
     $("footer, #menu-pece").addClass("d-none") </script>';

}

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<?php if (is_single()) {
    echo '<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>';} ?>

<?php

 if (is_page('monografias')) {

 echo '<script src = "https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script> 
    
    <script src = "https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js" ></script >

<script src = "https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" ></script >

<script src = "https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js" ></script >';}
?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>

</body>

</html>

