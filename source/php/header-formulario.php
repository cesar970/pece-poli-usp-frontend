<!DOCTYPE html>
<html lang="pt_BR">

<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>

        <?php

        if (is_home()):

            echo 'Pece Poli USP';

        elseif (is_category()):

            single_cat_title();

        elseif (is_single()):

            single_post_title();

        elseif (is_page()):

            single_post_title();

        else:

            wp_title('', true);

        endif;

        ?>

    </title>

    <meta name="robots" content="index, follow"/>

    <style type="text/css">

        <?php

        $url = get_bloginfo('template_directory') . '/dist/css/style.css';

        $ch = curl_init();

        curl_setopt ($ch, CURLOPT_URL, $url);

        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);

        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

        $contents = curl_exec($ch);

        if(curl_errno($ch)):

            echo curl_error($ch);

            $contents = '';

        else:

            curl_close($ch);

        endif;

        if(!is_string($contents) || !strlen($contents)):



            $contents = '';

    endif;



    $contents = str_replace('../img/', get_bloginfo('template_directory') . '/dist/img/', $contents);



    $contents = str_replace('../fonts/', get_bloginfo('template_directory') . '/dist/fonts/', $contents);



    echo $contents;

    ?>

    </style>


    <?php wp_head();

    function logoChange() {

        if (is_front_page() || is_single() || is_page('videos')) {
            return get_stylesheet_directory_uri() . '/dist/img/logo.png';
        } else {
            return get_stylesheet_directory_uri() . '/dist/img/logo-blue.png';
        }
    }

    ?>


</head>

<body data-url="<?php echo get_the_permalink(); ?>">

<header id="menu-pece" class="pt-3">

    <div class="container">

        <div class="row align-items-center pb-3 justify-content-center">

            <div class="logo col-12 pr-0 text-center">

                <a href="<?php echo get_home_url(); ?>">

                    <img class="pr-3" src="<?php echo logoChange(); ?> " alt="Logo PECE - Escola Politécnica da USP">

                </a>

            </div>

        </div>

    </div>

</header>















