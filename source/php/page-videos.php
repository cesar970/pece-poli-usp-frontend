<?php get_header() ?>

<div class="videos">

    <div class="d-flex flex-wrap align-items-end ">

        <div class="side-title d-flex col-md-4 col-lg-3 pl-0 ">

            <div class="detail-yellow"></div>

            <h2 class="font-weight-bold text-white mb-4 mb-md-0">Vídeos</h2>

        </div>

        <div class="col-md-4">

            <p class="text-white">O vídeo e o áudio estão cada vez mais presentes em nossas salas de aula e como
                materiais de revisão,
                aprendizado e estudo fora da aula. Clique aqui e tenha acesso a um interessante acervo de apoio aos seus
                estudos.</p>

        </div>

    </div>

    <div class="">

    <div class="mt-5 w-100 p-4 owl-videos owl-carousel text-white owl-theme position-relative">

        <?php if (have_rows('videos')): ?>

            <?php while (have_rows('videos')): the_row();

                $videos = get_sub_field('video');

                $titulo = get_sub_field('titulo');

                ?>

                <div class="item">

                    <?= $videos; ?> <br>

                    <div class="mt-2 font-weight-bold pl-4">

                        <?= $titulo; ?>

                    </div>

                </div>

            <?php endwhile; ?>

        <?php endif; ?>

    </div>

    </div>

</div>

<?php get_footer() ?>
