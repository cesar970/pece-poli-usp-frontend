<?php get_header() ?>

<section>

    <div class="page-wrapper text-left">

        <div class="d-flex flex-wrap">

            <div class="col-lg-4 pl-0">

                <div class="side-title d-flex align-items-center col-md-7 pl-0">

                    <div class="detail-yellow"></div>

                    <h2 class="font-weight-bold color-blued mb-md-4 pb-0">Blog</h2>

                </div>

                <div class="col-12 ml-lg-5 pl-lg-5">

                    <p class="col-12">Nossos eventos estão abertos ao público e refletem a relevância de nossas
                        pesquisas e cursos. Clique no
                        evento desejado e saiba mais.</p>

                </div>

            </div>

            <div class="col-md-7 m-auto">

                <?php

                $wp_query = new WP_Query();

                query_posts(array('post_type' => 'post', 'showposts' => 1, 'paged' => $paged));

                if (have_posts()):

                while ($wp_query->have_posts()) :
                $wp_query->the_post(); ?>

                <div class="container">

                    <div class="content-all">

                        <div class="my-3 my-md-5">

                            <div class="content-posts d-flex flex-wrap">

                                <div class="img-post-blog col-lg-6"
                                     style="background: url('<?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                     echo $url; ?>') no-repeat center center;">

                                </div>

                                <div class="titulo-blog-post silk-font col-lg-6 bg-light py-4 px-4 d-flex flex-wrap">

                                    <div class="data color-grey">

                                        <?php echo get_the_date('j F, Y'); ?>

                                    </div>

                                    <h1 class="title-page color-grey mt-2 color-blued font-weight-bold ">

                                        <?php echo wp_trim_words(get_the_title(), 10); ?>

                                    </h1>

                                    <div class="content">

                                        <p>  <?php echo wp_trim_words(get_the_content(), 15); ?> </p>

                                    </div>

                                    <a href="<?php the_permalink(); ?>" class="color-blued font-weight-bold">LEIA MAIS<i
                                                class="color-brown fas ml-3 fa-arrow-right"></i></a>

                                </div>

                            </div>

                            <?php endwhile; else: ?>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="container pr-md-1">

        <div class="content-blog">

            <div id=" ">

                <div class="row justify-content-md-between justify-content-center">


                    <?php

                    $current_page_ids = array(get_the_ID());

                    $wp_query = new WP_Query();

                    query_posts(

                        array(
                            'post_type' => 'post',
                            'showposts' => 3,
                            'paged' => $paged,
                            'post_parent' => '0',
                            'post__not_in' => $current_page_ids

                        )
                    );

                    if (have_posts()):

                        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                            <div class="my-4 my-md-3 col-lg-4 col-md-6">

                                <a href="<?php the_permalink(); ?>" class="content-posts color-greyd">


                                    <div class="img-post-blog-two"
                                         style="background: url('<?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                         echo $url; ?>') no-repeat center center; background-size:cover; height: 250px;">


                                    </div>

                                    <div class="titulo-blog-post silk-font  bg-light py-4 px-4 d-flex flex-wrap">

                                        <div class="data color-grey">

                                            <?php echo get_the_date('j F, Y'); ?>

                                        </div>

                                        <h1 class="title-page color-grey mt-2 color-blued font-weight-bold ">

                                            <?php echo wp_trim_words(get_the_title(), 8); ?>

                                        </h1>


                                        <a href="<?php the_permalink(); ?>" class="color-blued font-weight-bold">LEIA
                                            MAIS<i class="color-brown fas ml-3 fa-arrow-right"></i></a>

                                    </div>


                                </a>

                            </div>

                        <?php endwhile; else: ?>

                    <?php endif; ?>

                </div>

                <div class="wp-navi text-center mb-5">

                    <?php wp_pagenavi(); ?>

                </div>

            </div>
        </div>

    </div>


</section>

<?php get_footer() ?>
