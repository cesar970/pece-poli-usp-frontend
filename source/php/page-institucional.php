<?php get_header() ?>

<div class="institucional mt-5">

    <div class="side-title d-flex align-items-center pl-0">

        <div class="detail-yellow"></div>

        <h2 class="font-weight-bold color-blued mb-md-4 pb-0">Institucional</h2>

    </div>

    <div class="d-flex align-items-center flex-wrap justify-content-between institucional-text">

        <div class="col-md-7 historico">

            <span class="font-weight-bold color-greyl">HISTÓRICO</span>

            <p>

                Pioneirismo, expertise e acima de tudo, coragem! Munidos destas particularidades e com a
                responsabilidade de
                preencher uma lacuna educacional, foi fundado em 1973, o Programa de Educação Continuada da Escola
                Politécnica da Universidade de São Paulo – PECE/Poli.

            </p>

            <p>

                Já em 1987, o PECE/Poli tornou-se um paradigma da educação continuada nacional, atuando como modelo
                de
                negócio para as unidades da USP e de outras universidades pares do País. Atualmente, o programa
                disponibiliza mais de 20 cursos de extensão e é uma referência nacionalmente comprovada pelo mais de
                40 mil
                profissionais, das mais variadas áreas como, advogados, arquitetos, financistas, magistrados,
                peritos,
                profissionais de informática, peritos, tecnólogos e claro, engenheiros que passaram pela PECE/Poli.
            </p>

            <p>
                O maior e melhor centro de educação do Brasil é a direção segura para

            </p>

            <p>quem deseja o crescimento profissional. Basta selecionar o curso que atenda às suas necessidades,
                elevando suas habilidades a novos níveis à medida que sua carreira progride.
            </p>

            <p>
                Cada programa inclui um Certificado de Conclusão da USP. O corpo docente é destaque, uma vez que os
                educadores são professores doutores da Poli/USP e trazem experiências do mundo real para a sala de
                aula, promovendo uma aprendizagem contínua e um resultado significativo ao seu perfil.
            </p>

            <p>
                O PECE/Poli também promove cursos in company com os especialistas mais tarimbados do mercado. Outro
                diferencial do programa é o Sistema Integrado de Biblioteca no Brasil e no exterior, um suporte
                indispensável com um grande e completo acervo, disponível aos alunos.
            </p>

        </div>

        <div class="col-md-5 text-right pr-md-0">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/foto-institucional.jpg">

        </div>

    </div>

    <div class="container-institucional mt-5 pt-md-3">

        <h2 class="text-center color-blued font-weight-bold mb-5">Estrutura Hierárquica</h2>

        <div class="d-flex flex-wrap">

            <div class="col-md-6">

                <p>O Programa de Educação Continuada da Escola Politécnica da Universidade de São Paulo - PECE é o braço
                    da
                    Diretoria da Escola Politécnica, responsável, primordialmente, pela difusão do conhecimento gerado
                    na
                    Poli através dos Cursos de Extensão Universitária.
                </p>

                <p>

                    O Programa atua também, como agente patrocinador, na realização de diversos eventos científicos de
                    iniciativa de professores e pesquisadores da Poli, bem como na viabilização da vinda de professores
                    estrangeiros, de reconhecimento consagrado pela comunidade científica.
                </p>

                <p>
                    A partir de 1994 o PECE passa a ter na Escola Politécnica sua identidade estabelecida. A
                    regulamentação
                    formal e oficial do PECE foi definitivamente estabelecida em janeiro de 2007, conforme publicação de
                    Portaria n°. DIR-970/2007, da Diretoria da Escola Politécnica, em Diário Oficial do Estado, datado
                    em 05
                    de janeiro de 2007.
                </p>

            </div>

            <div class="col-md-5 bg-yellow color-blued py-5">

                <p>A condução do Programa é feita, desde o seu inicio, pelas figuras representadas pelo:</p>

                <ul class="list-estrutura">

                    <li> · Diretor da Escola Politécnica</li>

                    <li> · Vice-diretor da Escola Politécnica</li>

                    <li> · Coordenador geral do PECE</li>

                    <li> · O coordenador do PECE é sempre indicado
                        pela Diretoria da Escola Politécnica.
                    </li>

                </ul>

            </div>

        </div>

    </div>

    <div class="container-institucional bg-light py-4 py-md-5">

        <div class="col-12">

        <p>Além destas três figuras, o Programa solicita de todos os Departamentos da Escola Politécnica, a indicação de
            um
            coordenador para as atividades de cursos de extensão do
            referido departamento, docente este que atua como agente catalisador, junto a seus pares, na criação de
            cursos
            de extensão de interesse da comunidade.</p>

        <p>

            Dessa forma, o colegiado formado dita as diretrizes conceituais do Programa de Educação Continuada da Escola
            Politécnica.

        </p>

        </div>

    </div>

    <div class="container my-5 pb-md-4">

        <div class="row justify-content-center align-items-center">

            <div class="col-md-3">

                <h2 class="color-greyd font-weight-bold">Trabalhe <br>
                    conosco</h2>

                <div class="my-4 detail-yellow"></div>

            </div>

            <div class="col-md-8  text-white">

                <div class="bg-blueo p-md-4 p-2">

                    <p class="pb-4">

                    Junte-se à nossa apaixonada equipe de educadores e funcionários, dedicada a ajudar as pessoas a
                    progredirem em suas carreiras e alcançarem seus sonhos. Acesse o site da FUSP (www.fusp.org.br),
                    escolha a opção Processo Seletivo e siga as instruções do edital.
                    </p>

                    <span class="color-yellow font-weight-bold pt-4">Venha fazer parte deste time!</span>

                </div>

            </div>

        </div>

    </div>

</div>

<?php get_footer() ?>
