<?php get_header() ?>

<div class="page-wrapper">

    <div class="d-flex flex-wrap align-items-end">

        <div class="side-title d-flex col-lg-4 col-md-6 pl-0">

            <div class="detail-yellow"></div>

            <h2 class="font-weight-bold color-blued mb-4 mb-md-0">Eventos <br>
                e palestras</h2>

        </div>

        <div class="col-lg-7 col-md-6">

            <p>Nossos eventos estão abertos ao público e refletem a relevância de nossas pesquisas e cursos. Clique no
                evento desejado e saiba mais.</p>

        </div>

    </div>

    <div class="col-lg-10 pt-md-5 pt-3 m-auto content-eventos-palestras">

        <div class="position-relative row justify-content-center mt-md-4">

            <?php

            $wp_query = new WP_Query();

            query_posts(array('post_type' => 'eventosepalestras', 'showposts' => 6, 'paged' => $paged));

            if (have_posts()):

                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>


                    <?php
                    $terms = get_the_terms($post->ID, 'eventosepalestras');
                    foreach ($terms as $term) {

                    }
                    ?>

                    <a href="<?php the_permalink(); ?>" class="item mb-4 mx-md-4 mb-md-5 text-white <?php echo $term->name; ?>">

                        <div class="box-item">

                            <div class="schedule">

                                <small class="text-uppercase font-weight-bold d-flex align-items-center">

                                    <span class="detail-yellow d-inline-flex mr-2"></span>

                                    <?php echo $term->name; ?>

                                </small>

                                <div class="p-3">

                                    <div class="float-right info-event text-right">

                                        <span class="date"><?php the_field('data') ?></span> <br>

                                        <span class="time font-weight-bold mt-2"><?php the_field('horario') ?></span>

                                    </div>

                                    <div class="content-event w-100">

                                        <div class="title mb-4">

                                            <?php echo the_title(); ?>

                                        </div>

                                        <?php echo wp_trim_words(get_the_content(), 8, '...'); ?>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </a>

                <?php endwhile; else: ?>

            <?php endif; ?>

        </div>

        <?php include ('news-letter-widget.php')?>

    </div>

</div>

<?php get_footer() ?>
