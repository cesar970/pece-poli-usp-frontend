<?php get_header() ?>

<div class="faq">

    <div class="side-title d-flex align-items-center pl-0">

        <div class="detail-yellow"></div>

        <h2 class="font-weight-bold color-blued mb-md-4 pb-0">FAQ</h2>

    </div>

    <div class="container my-md-5 my-3">

        <div class="d-flex justify-content-around flex-wrap">

            <?php $collapse = ''; ?>

            <?php if (have_rows('categorias')): ?>

                <?php while (have_rows('categorias')):
                    the_row();

                    $categoriasNome = get_sub_field('nome');

                    $pergunta = get_sub_field('perguntas');

                    $indexRow = (get_row_index());

                    ?>

                    <a class="btn-collapse col-12 col-md-4 text-center my-2 my-md-0 " data-toggle="collapse"
                       data-icon="accordion-<?php echo get_row_index(); ?>"
                       href="#accordion-<?php echo get_row_index(); ?>"
                       role="button"
                       aria-expanded="false" data-tab="accordion-<?php echo get_row_index(); ?>"
                       aria-controls="accordion-<?php echo get_row_index(); ?>">
                        <?= $categoriasNome ?>
                    </a>

                    <?php

                    $collapse = $collapse . '<div class="collapse mt-3 mt-md-5" id="accordion-' . get_row_index() . '">';

                    if (have_rows('perguntas')):

                        while (have_rows('perguntas')): the_row();

                            $titulo = get_sub_field('titulo');

                            $descricao = get_sub_field('descricao');

                            $indexTab = get_row_index();

                            $collapse = $collapse . '

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                     <div class="panel-heading" role="tab" id="heading-' . $indexRow . $indexTab . '">
                        <div class="panel-title">
                            <a role="button" class="align-items-center" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse-' . $indexRow . $indexTab . '"
                               aria-expanded="false" aria-controls="collapseOne">
                               ' . $titulo . ' 
                            </a>
                        </div>
                    </div>
                    <div id="collapse-' . $indexRow . $indexTab . '"
                         class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="heading-' . $indexRow . $indexTab . '">
                        <div class="panel-body">
                             ' . $descricao . '
                        </div>

                    </div>

                </div>

            </div>

        ';

                        endwhile;

                    endif;

                    $collapse = $collapse . '</div>';

                    ?>

                <?php endwhile; ?>

            <?php endif; ?>

        </div>

        <div class="container mb-5">

            <?= $collapse ?>

        </div>

    </div>

    <?php get_footer() ?>
