<?php

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);

(add_theme_support('post-thumbnails'));


// include custom jQuery
function shapeSpace_include_custom_jquery()
{

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . "/source/components/jquery/dist/jquery.js", array(), null, true);

}

add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');

/**
 * Custom Post MBA, Especialização, EAD, Outros Cursos
 */


function tr_create_my_taxonomy() {

    register_taxonomy(
        'eventosepalestras',
        'eventosepalestras',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'eventosepalestras' ),
            'hierarchical' => true,
            'terms' => 'eventosepalestras'
        )
    );
}
add_action( 'init', 'tr_create_my_taxonomy' );



function create_post_type()
{


    /**
     * Eventos e Palestras
     */
    register_post_type('eventosepalestras',
        array(
            'labels' => array(
                'name' => __('Eventos e Palestras'),
                'singular_name' => __('Eventos e Palestras')
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon' => 'dashicons-welcome-learn-more',

        )
    );

    /**
     * MBA
     */
    register_post_type('mba',
        array(
            'labels' => array(
                'name' => __('MBA'),
                'singular_name' => __('MBA')
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon' => 'dashicons-welcome-learn-more',
        )
    );
    /**
     * Especialização
     */
    register_post_type('especializacao',
        array(
            'labels' => array(
                'name' => __('Especialização'),
                'singular_name' => __('Especialização'),
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon' => 'dashicons-welcome-learn-more',
        )
    );
    /**
     * EAD
     */
    register_post_type('ead',
        array(
            'labels' => array(
                'name' => __('EAD'),
                'singular_name' => __('EAD'),
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon' => 'dashicons-welcome-learn-more',
        )
    );
    /**
     * Outros Cursos
     */
    register_post_type('outroscursos',
        array(
            'labels' => array(
                'name' => __('Outros Cursos'),
                'singular_name' => __('Outros Cursos'),
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon' => 'dashicons-welcome-learn-more',
        )
    );
}

add_action('init', 'create_post_type');

?>






